from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/submit_form', methods=['POST'])
def submit_form():
    # Get form data
    cloud_provider = request.form.get('cloudProvider')
    cluster_name = request.form.get('clusterName')
    region = request.form.get('region')
    node_count = request.form.get('nodeCount')

    # Write form data to terraform.vars file
    with open('terraform.tfvars', 'w') as f:
        f.write(f'cloud_provider = "{cloud_provider}"\n')
        f.write(f'cluster_name = "{cluster_name}"\n')
        f.write(f'region = "{region}"\n')
        f.write(f'node_count = {node_count}\n')

    return 'Form submitted successfully!'

if __name__ == '__main__':
    app.run(debug=True)